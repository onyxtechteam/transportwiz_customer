import 'package:transportwiz_user/constants/api.dart';
import 'package:transportwiz_user/models/api_response.dart';
import 'package:transportwiz_user/models/category.dart';
import 'package:transportwiz_user/services/http.service.dart';

class CategoryRequest extends HttpService {
  //
  Future<List<Category>> categories({
    int vendorTypeId,
  }) async {
    final apiResult = await get(

      // 
      Api.categories,
      queryParameters: {
        "vendor_type_id": vendorTypeId,
      },
    );

    final apiResponse = ApiResponse.fromResponse(apiResult);

    if (apiResponse.allGood) {
      return apiResponse.data
          .map((jsonObject) => Category.fromJson(jsonObject))
          .toList();
    } else {
      throw apiResponse.message;
    }
  }
}
