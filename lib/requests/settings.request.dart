import 'package:transportwiz_user/constants/api.dart';
import 'package:transportwiz_user/models/api_response.dart';
import 'package:transportwiz_user/services/http.service.dart';

class SettingsRequest extends HttpService {
  //
  Future<ApiResponse> appSettings() async {
    final apiResult = await get(
      Api.appSettings
    );
    return ApiResponse.fromResponse(apiResult);
  }

}
