import 'package:flutter/material.dart';
import 'package:transportwiz_user/constants/app_colors.dart';
import 'package:transportwiz_user/translations/vendor/top_vendor.i18n.dart';
import 'package:velocity_x/velocity_x.dart';

class DeliveryTag extends StatelessWidget {
  const DeliveryTag({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return "Delivery"
                    .i18n
                    .text
                    .sm
                    .white
                    .make()
                    .py2()
                    .px8()
                    .box
                    .roundedLg
                    .color(AppColor.deliveryColor)
                    .make();
  }
}
