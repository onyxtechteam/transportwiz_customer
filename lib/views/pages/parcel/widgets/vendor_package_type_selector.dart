import 'package:flutter/material.dart';
import 'package:transportwiz_user/translations/new_parcel.i18n.dart';
import 'package:transportwiz_user/view_models/new_parcel.vm.dart';
import 'package:transportwiz_user/views/pages/parcel/widgets/form_step_controller.dart';
import 'package:transportwiz_user/widgets/custom_list_view.dart';
import 'package:transportwiz_user/widgets/list_items/parcel_vendor.list_item.dart';
import 'package:transportwiz_user/widgets/states/vendor.empty.dart';
import 'package:velocity_x/velocity_x.dart';

class VendorPackageTypeSelector extends StatelessWidget {
  const VendorPackageTypeSelector({this.vm, Key key}) : super(key: key);

  final NewParcelViewModel vm;
  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        //
        "Select Courier Vendor".i18n.text.xl.medium.make().py20(),
        //package type
        CustomListView(
          isLoading: vm.busy(vm.vendors),
          dataSet: vm.vendors,
          emptyWidget: EmptyVendor(showDescription: false),
          noScrollPhysics: true,
          itemBuilder: (context, index) {
            //
            final vendor = vm.vendors[index];
            return ParcelVendorListItem(
              vendor,
              selected: vm.selectedVendor == vendor,
              onPressed: () => vm.changeSelectedVendor(vendor),
            );
          },
        ).box.make().scrollVertical().expand(),

        //
        FormStepController(
          onPreviousPressed: () => vm.nextForm(0),
          onNextPressed:
              vm.selectedVendor != null ? () => vm.nextForm(2) : null,
        ),
      ],
    );
  }
}
