import 'package:flutter/material.dart';
import 'package:transportwiz_user/models/vendor_type.dart';
import 'package:transportwiz_user/view_models/vendor/categories.vm.dart';
import 'package:transportwiz_user/widgets/custom_horizontal_list_view.dart';
import 'package:transportwiz_user/widgets/list_items/category.list_item.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_user/translations/vendor/categories.i18n.dart';

class Categories extends StatelessWidget {
  const Categories(this.vendorType, {Key key}) : super(key: key);
  final VendorType vendorType;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CategoriesViewModel>.reactive(
      viewModelBuilder: () => CategoriesViewModel(
        context,
        vendorType: vendorType,
      ),
      onModelReady: (model) => model.initialise(),
      builder: (context, model, child) {
        return VStack(
          [
            //
            "Categories".i18n.text.xl.semiBold.make().p12(),

            //categories list
            CustomHorizontalListView(
              itemsViews: model.categories.map(
                (category) {
                  return CategoryListItem(
                    category: category,
                    onPressed: model.categorySelected,
                  );
                },
              ).toList(),
            ).pOnly(bottom: Vx.dp32),
          ],
        );
      },
    );
  }
}
