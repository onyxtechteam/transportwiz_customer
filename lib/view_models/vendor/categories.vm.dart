import 'package:flutter/material.dart';
import 'package:transportwiz_user/constants/app_routes.dart';
import 'package:transportwiz_user/models/category.dart';
import 'package:transportwiz_user/models/search.dart';
import 'package:transportwiz_user/models/vendor_type.dart';
import 'package:transportwiz_user/requests/category.request.dart';
import 'package:transportwiz_user/view_models/base.view_model.dart';
import 'package:velocity_x/velocity_x.dart';

class CategoriesViewModel extends MyBaseViewModel {
  CategoriesViewModel(BuildContext context, {this.vendorType}) {
    this.viewContext = context;
  }

  //
  CategoryRequest _categoryRequest = CategoryRequest();

  //
  List<Category> categories = [];
  final VendorType vendorType;

  //
  initialise() async {
    setBusy(true);
    try {
      categories = await _categoryRequest.categories(
        vendorTypeId: vendorType.id,
      );
      clearErrors();
    } catch (error) {
      setError(error);
    }
    setBusy(false);
  }

  //
  categorySelected(Category category) async {
    viewContext.navigator.pushNamed(
      AppRoutes.search,
      arguments: Search(category: category),
    );
  }
}
