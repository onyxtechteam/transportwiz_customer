import 'package:flutter/material.dart';
import 'package:transportwiz_user/constants/app_routes.dart';
import 'package:transportwiz_user/models/product.dart';
import 'package:transportwiz_user/models/vendor_type.dart';
import 'package:transportwiz_user/requests/product.request.dart';
import 'package:transportwiz_user/view_models/base.view_model.dart';
import 'package:velocity_x/velocity_x.dart';

class ForYouProductsViewModel extends MyBaseViewModel {
  //
  ProductRequest _productRequest = ProductRequest();
  //
  List<Product> products = [];
  VendorType vendorType;

  ForYouProductsViewModel(BuildContext context, this.vendorType) {
    this.viewContext = context;
  }

  //
  initialise() async {
    setBusy(true);
    try {
      products = await _productRequest.forYouProductsRequest(
        queryParams: {
          "vendor_type_id": vendorType.id,
        },
      );
      clearErrors();
    } catch (error) {
      setError(error);
    }
    setBusy(false);
  }

  //
  productSelected(Product product) async {
    viewContext.navigator.pushNamed(
      AppRoutes.product,
      arguments: product,
    );
  }
}
