import 'package:flutter/material.dart';
import 'package:transportwiz_user/constants/app_routes.dart';
import 'package:transportwiz_user/models/category.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:transportwiz_user/models/product.dart';
import 'package:transportwiz_user/constants/app_strings.dart';
import 'package:transportwiz_user/models/vendor.dart';
import 'package:transportwiz_user/requests/product.request.dart';
import 'package:transportwiz_user/view_models/base.view_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:velocity_x/velocity_x.dart';

class VendorCategoryProductsViewModel extends MyBaseViewModel {
  //
  VendorCategoryProductsViewModel(
    BuildContext context,
    this.category,
    this.vendor,
  ) {
    this.viewContext = context;
  }

  ProductRequest _productRequest = ProductRequest();
  RefreshController refreshContoller = RefreshController();

  //
  Category category;
  Vendor vendor;
  Map<int, List> categoriesProducts = {};
  Map<int, int> categoriesProductsQueryPages = {};
  final currencySymbol = AppStrings.currencySymbol;

  initialise() {
    //
    category.subcategories.forEach((element) {
      loadMoreProducts(element.id);
      categoriesProductsQueryPages[element.id] = 1;
    });
  }

  void productSelected(Product product) async {
    await viewContext.navigator.pushNamed(
      AppRoutes.product,
      arguments: product,
    );

    //
    notifyListeners();
  }

  loadMoreProducts(int id, {bool initialLoad = true}) async {
    int queryPage = categoriesProductsQueryPages[id] ?? 1;
    if (initialLoad) {
      queryPage = 1;
      categoriesProductsQueryPages[id] = queryPage;
      refreshContoller.refreshCompleted();
      setBusyForObject(id, true);
    } else {
      categoriesProductsQueryPages[id] = ++queryPage;
    }

    //load the products by subcategory id
    try {
      final mProducts = await _productRequest.getProdcuts(
        page: queryPage,
        queryParams: {
          "sub_category_id": id,
          "vendor_id": vendor?.id,
        },
      );

      //
      if (initialLoad) {
        categoriesProducts[id] = mProducts;
      } else {
        categoriesProducts[id].addAll(mProducts);
      }
    } catch (error) {}

    //
    if (initialLoad) {
      setBusyForObject(id, false);
    } else {
      refreshContoller.loadComplete();
    }

    //
    notifyListeners();
  }
}
